'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.findScrollOffset = exports.findModalAndGetOffset = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _lodash = require('lodash');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// Input: DOM element
// Returns: { int offsetLeft, int offsetTop }
// Determines how much the flexed parents have offset top and left.
var findModalAndGetOffset = exports.findModalAndGetOffset = function findModalAndGetOffset(element) {
  var offsetLeft = 0;
  var offsetTop = 0;
  var foundResult = false;
  var currentElement = element;
  do {
    // eslint-disable-line
    if (currentElement.className && currentElement.className.indexOf('c-modal__content') !== -1) {
      offsetTop = currentElement.offsetTop;
      offsetLeft = currentElement.offsetLeft;

      foundResult = true;
    }
  } while ((currentElement = currentElement.parentNode) !== null && !foundResult);
  return { offsetLeft: offsetLeft, offsetTop: offsetTop };
};

var findScrollOffset = exports.findScrollOffset = function findScrollOffset(element) {
  var scrollLeft = 0;
  var scrollTop = 0;
  var foundResult = false;
  var currentElement = element;
  do {
    if (currentElement.parentNode) {
      var elementRect = _reactDom2.default.findDOMNode(currentElement);
      scrollTop = scrollTop + elementRect.scrollTop;
      scrollLeft = scrollLeft + elementRect.scrollLeft;
    }
  } while ((currentElement = currentElement.parentNode) !== null);
  return { scrollLeft: scrollLeft, scrollTop: scrollTop };
};

// Fist place the tooltip below the children.
// If fails try above.
// TODO: Split the position up for every possible combination (this is a mess).
var getPosition = function getPosition(tooltip, contextMenu, alignProp) {
  var tooltipElement = _reactDom2.default.findDOMNode(tooltip).getBoundingClientRect();
  var contextMenuElement = _reactDom2.default.findDOMNode(contextMenu).getBoundingClientRect();

  var top = tooltipElement.top + tooltipElement.height;
  var left = tooltipElement.left + tooltipElement.width / 2;

  if (alignProp === 'right') {
    left = tooltipElement.left + tooltipElement.width;
  }

  if (alignProp === 'right-top') {
    left = tooltipElement.left + tooltipElement.width;
    top = tooltipElement.top;
  }

  var align = 'center';

  if (contextMenuElement.height + tooltipElement.height + tooltipElement.top > document.documentElement.clientHeight) {
    top = tooltipElement.top - contextMenuElement.height;
    align = 'center-bottom';

    if (alignProp === 'right-top') {
      top += tooltipElement.height * 2;
    }
  }

  var _findModalAndGetOffse = findModalAndGetOffset(tooltip),
      offsetLeft = _findModalAndGetOffse.offsetLeft,
      offsetTop = _findModalAndGetOffse.offsetTop;

  // Also move the drop down element to the left if not all of the element is on the screen.


  var pickerRightBorderLocation = contextMenuElement.left + contextMenuElement.width;

  if (document.documentElement.clientWidth - pickerRightBorderLocation < 0) {
    left -= document.documentElement.clientWidth - pickerRightBorderLocation;
  }

  return {
    align: align,
    style: {
      left: Math.round(left - offsetLeft),
      top: Math.round(top - offsetTop)
    }
  };
};

var classes = function classes(_ref, align) {
  var size = _ref.size,
      inverse = _ref.inverse,
      className = _ref.className,
      location = _ref.location,
      inline = _ref.inline,
      click = _ref.click;

  var _classNames;

  var active = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
  return (0, _classnames2.default)('c-tooltip', className, (_classNames = {}, _defineProperty(_classNames, 'c-tooltip_align-' + align, align), _defineProperty(_classNames, 'c-tooltip_location-' + location, location !== 'bottom'), _defineProperty(_classNames, 'c-tooltip_' + size, size !== 'regular'), _defineProperty(_classNames, 'c-tooltip_inverse', inverse), _defineProperty(_classNames, 'c-tooltip_inline', inline), _defineProperty(_classNames, 'js-clickable', click), _defineProperty(_classNames, 'js-active', active), _classNames));
};

var Tooltip = function (_Component) {
  _inherits(Tooltip, _Component);

  function Tooltip(props) {
    _classCallCheck(this, Tooltip);

    var _this = _possibleConstructorReturn(this, (Tooltip.__proto__ || Object.getPrototypeOf(Tooltip)).call(this, props));

    _this.setPosition = function () {
      var _getPosition = getPosition(_reactDom2.default.findDOMNode(_this.refTooltip), _this.contextMenu, _this.props.align),
          align = _getPosition.align,
          style = _getPosition.style;

      _this.setState({
        align: align,
        style: style
      });
    };

    _this.setLocation = function (e) {
      var contextMenu = _reactDom2.default.findDOMNode(_this.contextMenu);

      var top = e.nativeEvent.clientY;
      var align = 'center';

      if (contextMenu.getBoundingClientRect().height + e.nativeEvent.clientY > document.documentElement.clientHeight) {
        top = e.nativeEvent.clientY - contextMenu.getBoundingClientRect().height;
        align = 'center-bottom';
      }

      _this.setState({
        style: {
          left: Math.round(e.nativeEvent.clientX),
          top: Math.round(top)
        },
        align: align
      });
    };

    _this.setLocationBySelf = function (location) {
      var _findScrollOffset = findScrollOffset(_reactDom2.default.findDOMNode(_this.refTooltip)),
          scrollLeft = _findScrollOffset.scrollLeft,
          scrollTop = _findScrollOffset.scrollTop;

      switch (location) {
        case 'right':
          {
            _this.setState({
              style: {
                left: Math.round(_this.refTooltip.clientWidth - scrollLeft),
                top: 'inherit',
                marginTop: Math.round(-(_this.contextMenu.clientHeight / 2 + _this.refTooltip.clientHeight / 2 + scrollTop))
              },
              align: 'right'
            });
          }
      }
    };

    _this.handleBlur = function () {
      if (_this.props.hover) {
        _this.willHide = setTimeout(function () {
          _this.hide();
        }, _this.props.delayHide);
      }
    };

    _this.handleFocus = function () {
      _this.show();
    };

    _this.handleClick = function (e) {
      if (_this.props.click) {
        _this.show(e);
      }
    };

    _this.handleMouseOver = function () {
      var delayShow = _this.props.delayShow;


      if (!_this.props.hover) {
        return;
      }

      if (!delayShow) {
        _this.show();
        return;
      }

      _this.willShow = setTimeout(function () {
        _this.show();
      }, delayShow);
    };

    _this.handleMouseOut = function () {
      var delayHide = _this.props.delayHide;


      if (!_this.props.hover) {
        return;
      }

      if (!delayHide) {
        _this.hide();
        return;
      }

      _this.willHide = setTimeout(function () {
        _this.hide();
      }, _this.props.delayHide);
    };

    _this.handleDocumentClick = function (e) {
      var area = _reactDom2.default.findDOMNode(_this.refTooltip);

      if (!(0, _lodash.isEmpty)(area) && !area.contains(e.target)) {
        _this.hide();
      }
    };

    _this.handleContextMenu = function (e) {
      e.preventDefault();

      _this.show(e);
    };

    _this.hide = function () {
      if (_this.willShow) {
        clearTimeout(_this.willShow);
      }

      _this.setState({ visible: false });
    };

    _this.show = function (e) {
      if (_this.willHide) {
        clearTimeout(_this.willHide);
      }

      // If click is disabled but a focus or any other event triggers show this should not come to .
      if (!_this.props.click && !e) {
        return;
      }

      // The browser width of anything can change in the mean while so we need to recheck when the tooltip is shown.
      if (e && !_this.props.click) {
        // Check if you are clicking the context menu, then we don't need to change anything.
        if (!_this.contextMenu.contains(e.target)) {
          _this.setLocation(e);
        }
      } else {
        _this.setPosition();
        if (_this.props.location) {
          _this.setLocationBySelf(_this.props.location);
        }
      }

      _this.setState({ visible: true });
    };

    _this.state = {};
    return _this;
  }

  _createClass(Tooltip, [{
    key: 'componentWillMount',
    value: function componentWillMount() {
      if (!this.props.hover) {
        document.addEventListener('click', this.handleDocumentClick);
        document.addEventListener('contextmenu', this.handleDocumentClick);
        document.addEventListener('scroll', this.handleDocumentClick);
      }
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      if (this.props.content) {
        this.setPosition();
      }
    }
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps) {
      if (nextProps.content && nextProps.content !== this.props.content) {
        this.setPosition();
      }
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      if (!this.props.hover) {
        document.removeEventListener('click', this.handleDocumentClick);
        document.removeEventListener('contextmenu', this.handleDocumentClick);
        document.removeEventListener('scroll', this.handleDocumentClick);
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          children = _props.children,
          content = _props.content,
          contentClassName = _props.contentClassName,
          alignProps = _props.align,
          renderOnShow = _props.renderOnShow,
          className = _props.className;
      var _state = this.state,
          align = _state.align,
          style = _state.style;

      return _react2.default.createElement(
        'div',
        {
          ref: function ref(_ref3) {
            _this2.refTooltip = _ref3;
          },
          className: classes(this.props, alignProps || align, this.state.visible),
          onMouseOver: this.handleMouseOver,
          onMouseLeave: this.handleMouseOut,
          onFocus: this.handleFocus,
          onBlur: this.handleBlur,
          onClick: this.handleClick,
          onContextMenu: this.handleContextMenu
        },
        children,
        _react2.default.createElement(
          'div',
          {
            className: (0, _classnames2.default)('c-tooltip__contents', contentClassName),
            role: 'tooltip',
            'aria-hidden': !this.state.visible || !content,
            style: style,
            ref: function ref(_ref2) {
              _this2.contextMenu = _ref2;
            }
          },
          _react2.default.createElement(
            'div',
            { className: 'c-tooltip__content-wrapper' },
            (!renderOnShow || renderOnShow && this.state.visible) && content
          )
        )
      );
    }
  }]);

  return Tooltip;
}(_react.Component);

Tooltip.propTypes = {
  children: _propTypes2.default.node.isRequired,
  content: _propTypes2.default.node,
  delayShow: _propTypes2.default.number,
  delayHide: _propTypes2.default.number,
  hover: _propTypes2.default.bool,
  renderOnShow: _propTypes2.default.bool,
  inline: _propTypes2.default.bool, // eslint-disable-line
  click: _propTypes2.default.bool,
  inverse: _propTypes2.default.bool, // eslint-disable-line
  className: _propTypes2.default.string, // eslint-disable-line
  contentClassName: _propTypes2.default.string,
  align: _propTypes2.default.oneOf(['right', 'center', 'right-top', 'left']),
  size: _propTypes2.default.oneOf(['no-padding', 'small', 'regular', 'big']), // eslint-disable-line
  location: _propTypes2.default.oneOf(['bottom', 'right', 'right-bottom']) // eslint-disable-line
};

Tooltip.defaultProps = {
  delayHide: 200,
  size: 'regular',
  hover: true,
  click: true,
  inline: false,
  inverse: false,
  closeOnBlur: true,
  renderOnShow: false,
  location: 'bottom'
};

exports.default = Tooltip;