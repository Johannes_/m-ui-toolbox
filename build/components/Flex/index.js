'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames2 = require('classnames');

var _classnames3 = _interopRequireDefault(_classnames2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var Wrapper = function Wrapper(_ref) {
  var _classnames;

  var children = _ref.children,
      size = _ref.size,
      className = _ref.className,
      wrap = _ref.wrap,
      collapse = _ref.collapse,
      modifier = _ref.modifier,
      align = _ref.align,
      noPadding = _ref.noPadding;
  return _react2.default.createElement(
    'div',
    { className: (0, _classnames3.default)('c-flex', className, (_classnames = {}, _defineProperty(_classnames, 'c-flex_size-' + size, size), _defineProperty(_classnames, 'c-flex_collapse-' + collapse, collapse), _defineProperty(_classnames, 'c-flex_' + modifier, modifier), _defineProperty(_classnames, 'c-flex_align-' + align, align), _defineProperty(_classnames, 'c-flex_wrap', wrap), _defineProperty(_classnames, 'c-flex_no-padding', noPadding), _classnames))
    },
    children
  );
};

Wrapper.propTypes = {
  children: _propTypes2.default.node,
  size: _propTypes2.default.oneOf(['small', 'large']),
  className: _propTypes2.default.string,
  wrap: _propTypes2.default.bool,
  modifier: _propTypes2.default.oneOf(['row', 'centered-row', 'row-reversed', 'block', 'stretch', 'horizontal-separated']),
  noPadding: _propTypes2.default.bool,
  align: _propTypes2.default.oneOf(['flex-start']),
  collapse: _propTypes2.default.oneOf(['small-1', 'small-2', 'large-1'])
};

exports.default = Wrapper;