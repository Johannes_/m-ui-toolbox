'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames2 = require('classnames');

var _classnames3 = _interopRequireDefault(_classnames2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var Cell = function Cell(_ref) {
  var _classnames;

  var children = _ref.children,
      size = _ref.size,
      className = _ref.className,
      modifier = _ref.modifier,
      width = _ref.width,
      justify = _ref.justify;
  return _react2.default.createElement(
    'div',
    { className: (0, _classnames3.default)('c-flex__cell', className, (_classnames = {}, _defineProperty(_classnames, 'c-flex__size-' + size + '_cell', size), _defineProperty(_classnames, 'c-flex__width-' + width + '_cell', width), _defineProperty(_classnames, 'c-flex__justify-' + justify + '_cell', justify), _defineProperty(_classnames, 'c-flex__' + modifier + '_cell', modifier), _classnames))
    },
    children
  );
};

Cell.propTypes = {
  children: _propTypes2.default.node,
  size: _propTypes2.default.oneOf(['small', 'large']),
  className: _propTypes2.default.string,
  width: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]),
  modifier: _propTypes2.default.oneOf(['not-grow', 'not-shrink']),
  justify: _propTypes2.default.oneOf(['center']),
  padded: _propTypes2.default.bool
};

exports.default = Cell;