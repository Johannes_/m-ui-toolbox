'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.sizes = exports.themes = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _Icon = require('./Icon');

var _Icon2 = _interopRequireDefault(_Icon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// Components


// Assets
var iconProgress = function iconProgress(props) {
  return _react2.default.createElement(
    'svg',
    props,
    _react2.default.createElement(
      'title',
      null,
      'icon/small/progress'
    ),
    _react2.default.createElement(
      'g',
      {
        transform: 'translate(0 7)',
        stroke: '#000',
        fill: 'none',
        fillRule: 'evenodd'
      },
      _react2.default.createElement('circle', {
        cx: '1.5',
        cy: '1.5',
        r: '1'
      }),
      _react2.default.createElement('circle', {
        cx: '6.5',
        cy: '1.5',
        r: '1'
      }),
      _react2.default.createElement('circle', {
        cx: '11.5',
        cy: '1.5',
        r: '1'
      })
    )
  );
};

iconProgress.defaultProps = {
  width: '13',
  height: '13',
  viewBox: '0 0 13 13',
  xmlns: 'http://www.w3.org/2000/svg'
};


var classes = function classes(_ref, theme, appearance, modifier, hoverIcon, state) {
  var size = _ref.size,
      loading = _ref.loading,
      className = _ref.className;

  var _classNames;

  return (0, _classnames2.default)((_classNames = {
    'c-button': appearance === 'button'
  }, _defineProperty(_classNames, 'c-button_' + size, size !== 'regular'), _defineProperty(_classNames, 'c-button_hover-icon', hoverIcon), _defineProperty(_classNames, 'c-button_' + modifier, modifier), _defineProperty(_classNames, 'c-button_appearance-' + appearance, appearance !== 'button'), _defineProperty(_classNames, 'c-button_theme-' + theme, theme !== 'primary'), _defineProperty(_classNames, 'is-loading', loading), _classNames), className, state);
};

var themes = exports.themes = ['primary', 'secondary', 'danger', 'info', 'inverted', 'normal', 'theme-brand-ghost'];

var sizes = exports.sizes = ['tiny', 'small', 'regular', 'large', 'medium-rounded'];

var Button = function (_Component) {
  _inherits(Button, _Component);

  function Button() {
    _classCallCheck(this, Button);

    return _possibleConstructorReturn(this, (Button.__proto__ || Object.getPrototypeOf(Button)).apply(this, arguments));
  }

  _createClass(Button, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          disabled = _props.disabled,
          loading = _props.loading,
          type = _props.type,
          children = _props.children,
          tag = _props.tag,
          theme = _props.theme,
          appearance = _props.appearance,
          modifier = _props.modifier,
          icon = _props.icon,
          hoverIcon = _props.hoverIcon,
          size = _props.size,
          _props$submitting = _props.submitting,
          submitting = _props$submitting === undefined ? false : _props$submitting,
          onClick = _props.onClick,
          state = _props.state,
          other = _objectWithoutProperties(_props, ['disabled', 'loading', 'type', 'children', 'tag', 'theme', 'appearance', 'modifier', 'icon', 'hoverIcon', 'size', 'submitting', 'onClick', 'state']);

      var iconComponent = icon && _react2.default.createElement(_Icon2.default, { data: icon, size: size, theme: theme, className: 'c-button__icon' });

      if (loading) {
        iconComponent = _react2.default.createElement(_Icon2.default, {
          key: 'active',
          className: 'c-button__icon',
          skipDefaultClass: true,
          data: iconProgress
        });
      }

      var wrappedChildren = Boolean(children) && _react2.default.createElement(
        'span',
        { className: 'c-button__children' },
        children
      );

      var Tag = tag;
      return _react2.default.createElement(
        Tag,
        _extends({}, other, {
          onClick: !disabled ? onClick : undefined,
          className: classes(this.props, theme, appearance, modifier, hoverIcon, state),
          disabled: disabled || loading,
          type: type
        }),
        submitting ? _react2.default.createElement(
          'span',
          null,
          wrappedChildren,
          _react2.default.createElement(_Icon2.default, {
            className: 'u-transform-rotate c-button__icon',
            skipDefaultClass: true,
            data: iconProgress
          })
        ) : _react2.default.createElement(
          'span',
          null,
          wrappedChildren,
          hoverIcon && _react2.default.createElement(_Icon2.default, {
            data: hoverIcon,
            size: size,
            theme: theme,
            className: 'c-button__icon c-button_hover_icon'
          }),
          iconComponent
        )
      );
    }
  }]);

  return Button;
}(_react.Component);

exports.default = Button;


Button.propTypes = {
  tag: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.func]),
  appearance: _propTypes2.default.oneOf(['button', 'link']),
  theme: _propTypes2.default.oneOf(themes),
  children: _propTypes2.default.node,
  icon: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.func]),
  hoverIcon: _propTypes2.default.node,
  disabled: _propTypes2.default.bool,
  submitting: _propTypes2.default.bool,
  loading: _propTypes2.default.bool,
  size: _propTypes2.default.oneOf(sizes),
  modifier: _propTypes2.default.oneOf(['wide', 'flat']),
  state: _propTypes2.default.oneOf(['static'])
};

Button.defaultProps = {
  tag: 'button',
  appearance: 'button',
  theme: 'primary',
  children: null,
  disabled: false,
  loading: false,
  type: 'submit'
};