'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _Icon = require('./Icon');

var _Icon2 = _interopRequireDefault(_Icon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// Components


// Imports
var userIcon = function userIcon(props) {
  return _react2.default.createElement(
    'svg',
    props,
    _react2.default.createElement(
      'title',
      null,
      'user'
    ),
    _react2.default.createElement(
      'g',
      {
        stroke: '#FFF',
        fill: 'none',
        fillRule: 'evenodd'
      },
      _react2.default.createElement('circle', {
        cx: '8.5',
        cy: '6.5',
        r: '3'
      }),
      _react2.default.createElement('path', {
        d: 'M15.5 16.5a7 7 0 0 0-14 0'
      })
    )
  );
};

userIcon.defaultProps = {
  width: '17',
  height: '17',
  viewBox: '0 0 17 17',
  xmlns: 'http://www.w3.org/2000/svg'
};

var starIcon = function starIcon(props) {
  return _react2.default.createElement(
    'svg',
    props,
    _react2.default.createElement(
      'title',
      null,
      'icon/small/star'
    ),
    _react2.default.createElement('path', {
      d: 'M8.5 12.252l-4.017 2.112.767-4.474L2 6.723l4.491-.653L8.5 2l2.009 4.07L15 6.723 11.75 9.89l.767 4.474z',
      stroke: '#000',
      fill: 'none',
      fillRule: 'evenodd'
    })
  );
};

starIcon.defaultProps = {
  width: '17',
  height: '16',
  viewBox: '0 0 17 16',
  xmlns: 'http://www.w3.org/2000/svg'
};


var classes = function classes(theme, size, wrapperClasses, imageError, modifier, color) {
  var _classNames;

  return (0, _classnames2.default)('c-avatar', (_classNames = {}, _defineProperty(_classNames, 'c-avatar_' + theme, theme), _defineProperty(_classNames, 'c-avatar_' + modifier, modifier), _defineProperty(_classNames, 'c-avatar_size-' + size, size), _defineProperty(_classNames, 'js-image-error', imageError), _defineProperty(_classNames, 'js-active', color), _classNames), wrapperClasses);
};

var Avatar = function (_Component) {
  _inherits(Avatar, _Component);

  function Avatar(props) {
    _classCallCheck(this, Avatar);

    var _this = _possibleConstructorReturn(this, (Avatar.__proto__ || Object.getPrototypeOf(Avatar)).call(this, props));

    _this.handleImageError = function () {
      _this.setState({
        imageError: true
      });
    };

    _this.state = { imageError: false };
    return _this;
  }

  _createClass(Avatar, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          image = _props.image,
          alt = _props.alt,
          theme = _props.theme,
          size = _props.size,
          wrapperClasses = _props.wrapperClasses,
          _props$className = _props.className,
          className = _props$className === undefined ? '' : _props$className,
          width = _props.width,
          height = _props.height,
          nickname = _props.nickname,
          modifier = _props.modifier,
          color = _props.color,
          isSpecial = _props.isSpecial;

      var addedStyle = {};

      if (color) {
        addedStyle.borderColor = color;
        addedStyle.backgroundColor = color;
      }

      return _react2.default.createElement(
        'span',
        {
          className: classes(theme, size, wrapperClasses, this.state.imageError, modifier, color),
          style: addedStyle
        },
        isSpecial && imageError && _react2.default.createElement(_Icon2.default, {
          className: 'c-avatar__very-special-icon',
          width: width,
          height: height,
          data: starIcon,
          theme: theme,
          size: size,
          style: { stroke: color }
        }),
        image && _react2.default.createElement('img', {
          onError: this.handleImageError,
          className: 'c-avatar__image ' + className,
          width: width,
          height: height,
          alt: alt,
          src: image
        })
      );
    }
  }]);

  return Avatar;
}(_react.Component);

Avatar.propTypes = {
  image: _propTypes2.default.oneOfType([_propTypes2.default.object, _propTypes2.default.string]),
  theme: _propTypes2.default.oneOf(['inverted']),
  modifier: _propTypes2.default.oneOf(['activatable']),
  size: _propTypes2.default.oneOf(['small', 'large', 'big', 'full']),
  wrapperClasses: _propTypes2.default.string,
  nickname: _propTypes2.default.string,
  alt: _propTypes2.default.string,
  className: _propTypes2.default.string,
  height: _propTypes2.default.string,
  color: _propTypes2.default.oneOfType([_propTypes2.default.bool, _propTypes2.default.string]),
  isSpecial: _propTypes2.default.bool,
  width: _propTypes2.default.string
};

exports.default = Avatar;