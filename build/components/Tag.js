'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _Icon = require('./Icon');

var _Icon2 = _interopRequireDefault(_Icon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// Components


var classes = function classes(size, loading, className, theme, modifier) {
  var _classNames;

  return (0, _classnames2.default)('c-tag', (_classNames = {}, _defineProperty(_classNames, 'c-tag_size-' + size, size), _defineProperty(_classNames, 'c-tag_theme-' + theme, theme), _defineProperty(_classNames, 'c-tag_' + modifier, modifier), _classNames), className);
};

var Tag = function Tag(props) {
  var children = props.children,
      _props$theme = props.theme,
      theme = _props$theme === undefined ? 'normal' : _props$theme,
      icon = props.icon,
      addOn = props.addOn,
      size = props.size,
      className = props.className,
      modifier = props.modifier,
      title = props.title,
      _props$addOnLocationB = props.addOnLocationBehind,
      addOnLocationBehind = _props$addOnLocationB === undefined ? false : _props$addOnLocationB,
      style = props.style;


  var addOns = [];

  if (icon) {
    addOns.push(_react2.default.createElement(_Icon2.default, {
      key: 'tab-icon',
      className: 'c-tag__icon',
      skipDefaultClass: true,
      data: icon,
      size: size
    }));
  }

  if (addOn) {
    addOns.push(_react2.default.createElement(
      'span',
      { key: 'tab-add-on', className: 'c-tag__add-on' },
      addOn
    ));
  }

  return _react2.default.createElement(
    'span',
    { className: classes(size, icon, className, theme, modifier), style: style },
    !addOnLocationBehind && addOns,
    _react2.default.createElement(
      'span',
      { className: 'c-tag__body' },
      title,
      children
    ),
    addOnLocationBehind && addOns
  );
};

Tag.propTypes = {
  theme: _propTypes2.default.oneOf(['primary', 'normal', 'secondary', 'danger', 'warning', 'info', 'success']),
  style: _propTypes2.default.object,
  children: _propTypes2.default.node,
  icon: _propTypes2.default.node,
  modifier: _propTypes2.default.string,
  title: _propTypes2.default.string,
  addOn: _propTypes2.default.string,
  className: _propTypes2.default.string,
  addOnLocationBehind: _propTypes2.default.bool,
  size: _propTypes2.default.oneOf(['small', 'medium', 'large'])
};

exports.default = Tag;