'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactModal = require('react-modal');

var _reactModal2 = _interopRequireDefault(_reactModal);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var classes = function classes(className, size) {
  return (0, _classnames2.default)('c-modal__content', className, _defineProperty({}, 'c-modal_' + size + '_content', size));
};

var overlayClasses = function overlayClasses(overlayClassName, position) {
  return (0, _classnames2.default)('c-modal', overlayClassName, _defineProperty({}, 'c-modal_position-' + position, position));
};

var Modal = function (_Component) {
  _inherits(Modal, _Component);

  function Modal(props) {
    _classCallCheck(this, Modal);

    var _this = _possibleConstructorReturn(this, (Modal.__proto__ || Object.getPrototypeOf(Modal)).call(this, props));

    _this.state = {
      savedOffsetX: 0,
      savedOffsetY: 0
    };
    return _this;
  }

  _createClass(Modal, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          children = _props.children,
          className = _props.className,
          isOpen = _props.isOpen,
          overlayClassName = _props.overlayClassName,
          portalClassName = _props.portalClassName,
          position = _props.position,
          _props$offsetX = _props.offsetX,
          offsetX = _props$offsetX === undefined ? 0 : _props$offsetX,
          _props$offsetY = _props.offsetY,
          offsetY = _props$offsetY === undefined ? 0 : _props$offsetY,
          handleClose = _props.handleClose,
          size = _props.size,
          otherProps = _objectWithoutProperties(_props, ['children', 'className', 'isOpen', 'overlayClassName', 'portalClassName', 'position', 'offsetX', 'offsetY', 'handleClose', 'size']);

      return _react2.default.createElement(
        _reactModal2.default,
        _extends({
          className: classes(className, size),
          overlayClassName: overlayClasses(overlayClassName, position),
          portalClassName: (0, _classnames2.default)('c-modal__portal', portalClassName),
          onRequestClose: handleClose,
          style: {
            overlay: {
              left: offsetX > 0 && offsetX,
              top: offsetY > 0 && offsetY
            }
          },
          isOpen: isOpen,
          contentLabel: 'content'
        }, otherProps),
        children
      );
    }
  }]);

  return Modal;
}(_react.Component);

Modal.propTypes = {
  children: _propTypes2.default.node.isRequired,
  isOpen: _propTypes2.default.bool.isRequired,
  className: _propTypes2.default.string,
  overlayClassName: _propTypes2.default.string,
  offsetX: _propTypes2.default.number,
  offsetY: _propTypes2.default.number,
  portalClassName: _propTypes2.default.string,
  handleClose: _propTypes2.default.func,
  size: _propTypes2.default.oneOf(['small']),
  position: _propTypes2.default.oneOf(['left']) // default is centered
};

exports.default = Modal;