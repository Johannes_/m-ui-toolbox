'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactIntl = require('react-intl');

var _Cell = require('../Flex/Cell');

var _Cell2 = _interopRequireDefault(_Cell);

var _Flex = require('../Flex');

var _Flex2 = _interopRequireDefault(_Flex);

var _Content = require('../Content');

var _Content2 = _interopRequireDefault(_Content);

var _Button = require('../Button');

var _Button2 = _interopRequireDefault(_Button);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var crossIcon = function crossIcon(props) {
	return _react2.default.createElement(
		'svg',
		props,
		_react2.default.createElement(
			'title',
			null,
			'md-cross'
		),
		_react2.default.createElement(
			'g',
			{
				fill: 'none',
				fillRule: 'evenodd',
				stroke: '#2B2B2B'
			},
			_react2.default.createElement('path', {
				d: 'M.929 1.429L15.07 15.57M.929 15.571L15.07 1.43'
			})
		)
	);
};

crossIcon.defaultProps = {
	width: '16',
	height: '16',
	viewBox: '0 0 16 16',
	xmlns: 'http://www.w3.org/2000/svg'
};

var ModalHeader = function (_Component) {
	_inherits(ModalHeader, _Component);

	function ModalHeader() {
		_classCallCheck(this, ModalHeader);

		return _possibleConstructorReturn(this, (ModalHeader.__proto__ || Object.getPrototypeOf(ModalHeader)).apply(this, arguments));
	}

	_createClass(ModalHeader, [{
		key: 'render',
		value: function render() {
			var _props = this.props,
			    title = _props.title,
			    subTitle = _props.subTitle,
			    handleClose = _props.handleClose;


			return _react2.default.createElement(
				_Flex2.default,
				{ noPadding: true, align: 'flex-start' },
				_react2.default.createElement(
					_Cell2.default,
					null,
					_react2.default.createElement(
						_Content2.default,
						{ size: 'medium' },
						_react2.default.createElement(
							'h3',
							{ className: 'u-no-margin' },
							_react2.default.createElement(_reactIntl.FormattedMessage, title)
						),
						subTitle
					)
				),
				_react2.default.createElement(
					_Cell2.default,
					{ modifier: 'not-grow' },
					_react2.default.createElement(
						_Content2.default,
						{ size: 'medium' },
						_react2.default.createElement(_Button2.default, { icon: crossIcon, onClick: handleClose, appearance: 'link' })
					)
				)
			);
		}
	}]);

	return ModalHeader;
}(_react.Component);

ModalHeader.propTypes = {
	title: _propTypes2.default.object,
	handleClose: _propTypes2.default.func,
	subTitle: _propTypes2.default.node
};

exports.default = ModalHeader;