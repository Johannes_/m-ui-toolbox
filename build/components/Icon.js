'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactSvgInline = require('react-svg-inline');

var _reactSvgInline2 = _interopRequireDefault(_reactSvgInline);

var _classnames2 = require('classnames');

var _classnames3 = _interopRequireDefault(_classnames2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var classes = function classes(className, theme, size, modifier) {
  var _classnames;

  return (0, _classnames3.default)('c-icon', (_classnames = {}, _defineProperty(_classnames, 'c-icon_' + theme, theme !== 'primary'), _defineProperty(_classnames, 'c-icon_size-' + size, size), _defineProperty(_classnames, 'c-icon_' + modifier, modifier), _classnames), className);
};

var Icon = function Icon(_ref) {
  var data = _ref.data,
      _ref$className = _ref.className,
      className = _ref$className === undefined ? '' : _ref$className,
      style = _ref.style,
      theme = _ref.theme,
      size = _ref.size,
      width = _ref.width,
      height = _ref.height,
      modifier = _ref.modifier;

  if (typeof data === "string") {
    if (data.startsWith('/static') || data.startsWith('data:image/svg') || data.endsWith('.svg')) {

      return _react2.default.createElement('img', {
        src: data,
        className: classes(className, theme, size, modifier),
        style: style,
        width: width,
        height: height
      });
    }
    return _react2.default.createElement(_reactSvgInline2.default, {
      className: classes(className, theme, size, modifier),
      svg: data,
      style: style,
      width: width,
      height: height
    });
  }
  if (data) {
    return _react2.default.createElement(
      _react.Fragment,
      null,
      _react2.default.createElement(
        'span',
        { className: classes(className, theme, size, modifier) },
        data()
      )
    );
  }
  return null;
};

Icon.propTypes = {
  data: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.func]).isRequired,
  width: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]),
  height: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]),
  modifier: _propTypes2.default.string,
  theme: _propTypes2.default.oneOf(['primary', 'secondary', 'normal', 'inline', 'danger', 'normal-inline', 'rotate']),
  size: _propTypes2.default.oneOf(['tiny', 'small', 'large', 'medium-rounded', 'full', 'extra-large']),
  className: _propTypes2.default.string,
  style: _propTypes2.default.object
};
exports.default = Icon;