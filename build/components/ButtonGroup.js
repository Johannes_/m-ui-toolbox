'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var classes = function classes(className, modifier) {
  return (0, _classnames2.default)('c-button-group', _defineProperty({}, 'c-button-group_' + modifier, modifier), className);
};

var ButtonGroup = function ButtonGroup(props) {
  var children = props.children,
      className = props.className,
      modifier = props.modifier;


  return _react2.default.createElement(
    'span',
    { className: classes(className, modifier) },
    children
  );
};

ButtonGroup.propTypes = {
  children: _propTypes2.default.node,
  modifier: _propTypes2.default.string,
  className: _propTypes2.default.string
};

exports.default = ButtonGroup;