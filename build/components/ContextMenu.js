'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _lodash = require('lodash');

var _reactIntl = require('react-intl');

var _Tooltip = require('./Tooltip');

var _Tooltip2 = _interopRequireDefault(_Tooltip);

var _Icon = require('./Icon');

var _Icon2 = _interopRequireDefault(_Icon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// Components


var classes = function classes(className, block, pullRight) {
  return (0, _classnames2.default)('c-context-menu', {
    'u-block': block,
    'u-pull-right': pullRight
  }, className);
};

var actionClasses = function actionClasses(_ref) {
  var _classNames;

  var modifier = _ref.modifier,
      className = _ref.className,
      handleAction = _ref.handleAction,
      _ref$isAction = _ref.isAction,
      isAction = _ref$isAction === undefined ? false : _ref$isAction,
      isActive = _ref.isActive,
      pullRight = _ref.pullRight;
  return (0, _classnames2.default)('c-context-menu__item', className, (_classNames = {}, _defineProperty(_classNames, 'c-context-menu_' + modifier + '_item', modifier), _defineProperty(_classNames, 'c-context-menu_no-action_item', !handleAction && !isAction), _defineProperty(_classNames, 'c-context-menu_button_item', modifier === 'submit'), _defineProperty(_classNames, 'js-active', isActive), _classNames));
};
var contentClasses = function contentClasses(_ref2) {
  var _ref2$align = _ref2.align,
      align = _ref2$align === undefined ? 'left' : _ref2$align;
  return (0, _classnames2.default)('c-context-menu__contents', _defineProperty({}, 'c-context-menu_align-' + align + '_contents', align));
};

var ContextMenu = function (_Component) {
  _inherits(ContextMenu, _Component);

  function ContextMenu() {
    _classCallCheck(this, ContextMenu);

    return _possibleConstructorReturn(this, (ContextMenu.__proto__ || Object.getPrototypeOf(ContextMenu)).apply(this, arguments));
  }

  _createClass(ContextMenu, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          children = _props.children,
          actionHeader = _props.actionHeader,
          align = _props.align,
          actions = _props.actions,
          className = _props.className,
          block = _props.block,
          form = _props.form,
          _props$hover = _props.hover,
          hover = _props$hover === undefined ? false : _props$hover,
          _props$click = _props.click,
          click = _props$click === undefined ? false : _props$click,
          _props$level = _props.level,
          level = _props$level === undefined ? 1 : _props$level,
          renderOnShow = _props.renderOnShow,
          _props$pullRight = _props.pullRight,
          pullRight = _props$pullRight === undefined ? false : _props$pullRight;


      var content = _react2.default.createElement(
        'span',
        null,
        actionHeader && _react2.default.createElement(
          'div',
          { className: 'c-context-menu__header' },
          actionHeader
        ),
        (0, _lodash.size)(actions) > 0 && actions.map(function (action, key) {
          var actionContent = void 0;

          if (action.modifier === 'submit') {
            actionContent = _react2.default.createElement(
              'div',
              { className: actionClasses(action) },
              _react2.default.createElement(
                'button',
                {
                  type: 'button',
                  className: 'c-context-menu__button c-button c-button_primary',
                  onClick: function onClick() {
                    if (!action.preventClose) {
                      // Tooltip on focus will keep the tooltip active. Doing it one frame later solves this.
                      requestAnimationFrame(function () {
                        _this2.tooltip.hide();
                      });
                    }
                  }
                },
                action.icon && _react2.default.createElement(_Icon2.default, { data: action.icon, size: 'small' }),
                action.label && action.label.id ? _react2.default.createElement(_reactIntl.FormattedMessage, action.label) : action.label
              )
            );
          } else {
            actionContent = _react2.default.createElement(
              'div',
              {
                className: actionClasses(action),
                onClick: function onClick(e) {
                  if (action.handleAction) {
                    action.handleAction(e);

                    if (!action.preventClose) {
                      _this2.tooltip.hide();
                    }
                  }
                }
              },
              action.icon && _react2.default.createElement(_Icon2.default, { data: action.icon, size: 'small' }),
              action.label && action.label.id ? _react2.default.createElement(_reactIntl.FormattedMessage, action.label) : action.label
            );
          }

          if (action.child) {
            return _react2.default.createElement(
              ContextMenu,
              _extends({
                modifier: 'inline',
                key: 'context-menu-action-' + level,
                block: true,
                align: 'right-top',
                hover: true,
                click: true
              }, action.child, {
                level: level ? level + 1 : 1
              }),
              actionContent
            );
          }

          return _react2.default.createElement(
            'span',
            { key: 'context-menu-action-' + level + '-' + key + '-' + action.id },
            actionContent
          );
        })
      );

      if (form) {
        content = _react2.default.createElement(
          'form',
          form,
          content
        );
      }

      return _react2.default.createElement(
        _Tooltip2.default,
        {
          size: 'no-padding',
          content: content,
          className: classes(className, block, pullRight),
          ref: function ref(_ref3) {
            _this2.tooltip = _ref3;
          },
          hover: hover,
          align: align,
          click: click,
          renderOnShow: renderOnShow,
          contentClassName: contentClasses(this.props)
        },
        children
      );
    }
  }]);

  return ContextMenu;
}(_react.Component);

ContextMenu.propTypes = {
  children: _propTypes2.default.node.isRequired,
  actionHeader: _propTypes2.default.string,
  align: _propTypes2.default.string,
  form: _propTypes2.default.object,
  block: _propTypes2.default.bool,
  hover: _propTypes2.default.bool,
  renderOnShow: _propTypes2.default.bool,
  click: _propTypes2.default.bool,
  className: _propTypes2.default.string,
  level: _propTypes2.default.number,
  actions: _propTypes2.default.array.isRequired
};

exports.default = ContextMenu;