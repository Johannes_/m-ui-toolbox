'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactIntl = require('react-intl');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var classes = function classes(className, modifier, size) {
  var _classNames;

  return (0, _classnames2.default)('c-add-on', className, (_classNames = {}, _defineProperty(_classNames, 'c-add-on_' + modifier, modifier), _defineProperty(_classNames, 'c-add-on_size-' + size, size), _classNames));
};

var AddOn = function AddOn(props) {
  var name = props.name,
      children = props.children,
      data = props.data,
      modifier = props.modifier,
      size = props.size,
      placeholder = props.placeholder,
      className = props.className,
      _props$position = props.position,
      position = _props$position === undefined ? 'left' : _props$position,
      formatMessage = props.intl.formatMessage;


  var placeholderProp = placeholder && placeholder.hasOwnProperty('id') ? formatMessage(placeholder) : placeholder;

  var content = _react2.default.createElement(
    'span',
    {
      className: classes(className, modifier, size),
      id: 'c-add-on-' + name,
      name: name,
      placeholder: placeholderProp
    },
    data
  );

  return position === 'left' ? _react2.default.createElement(
    'span',
    { className: 'c-add-on-wrapper' },
    content,
    children
  ) : _react2.default.createElement(
    'span',
    { className: 'c-add-on-wrapper' },
    children,
    content
  );
};

AddOn.propTypes = {
  className: _propTypes2.default.string,
  placeholder: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.object, _propTypes2.default.number, _propTypes2.default.shape({
    id: _propTypes2.default.string.isRequired,
    defaultMessage: _propTypes2.default.string.isRequired
  })]),
  name: _propTypes2.default.string,
  modifier: _propTypes2.default.string,
  data: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.node]),
  position: _propTypes2.default.oneOf(['left', 'right']),
  intl: _propTypes2.default.object
};

exports.default = (0, _reactIntl.injectIntl)(AddOn);