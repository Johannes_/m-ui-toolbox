'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames2 = require('classnames');

var _classnames3 = _interopRequireDefault(_classnames2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var classes = function classes(modifier, className) {
  var _classnames;

  return (0, _classnames3.default)("c-card", (_classnames = {}, _defineProperty(_classnames, 'c-card_' + modifier, modifier), _defineProperty(_classnames, className, className), _classnames));
};

var Card = function Card(_ref) {
  var children = _ref.children,
      className = _ref.className,
      modifier = _ref.modifier;
  return _react2.default.createElement(
    'div',
    { className: classes(modifier, className) },
    children
  );
};

Card.propTypes = {
  children: _propTypes2.default.node.isRequired,
  modifier: _propTypes2.default.oneOf(['as-link', 'fluid', 'bordered-content', 'bordered', 'inset', 'scale-down'])
};

exports.default = Card;