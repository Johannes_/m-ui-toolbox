'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var classes = function classes(_ref) {
  var _classNames;

  var modifier = _ref.modifier,
      className = _ref.className,
      size = _ref.size;
  return (0, _classnames2.default)("c-dropdown__item", className, (_classNames = {}, _defineProperty(_classNames, 'c-dropdown__' + modifier + '_item', modifier), _defineProperty(_classNames, 'c-dropdown__size-' + size + '_item', size), _classNames));
};

// This is a wrapper for content. So hardly any styling only paddings and max-widths all over the place.
var DropdownItem = function DropdownItem(_ref2) {
  var children = _ref2.children,
      rest = _objectWithoutProperties(_ref2, ['children']);

  return _react2.default.createElement(
    'div',
    { className: classes(rest) },
    children
  );
};

DropdownItem.propTypes = {
  children: _propTypes2.default.node.isRequired,
  modifier: _propTypes2.default.string,
  size: _propTypes2.default.oneOf(['none', 'tiny', 'small', 'large', 'extra-large']),
  inline: _propTypes2.default.bool,
  inverted: _propTypes2.default.bool,
  separate: _propTypes2.default.bool
};

exports.default = DropdownItem;