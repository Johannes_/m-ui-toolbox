'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var classes = function classes(_ref) {
  var _classesNames;

  var modifier = _ref.modifier,
      className = _ref.className,
      size = _ref.size;
  return (0, _classnames2.default)("c-dropdown", (_classesNames = {}, _defineProperty(_classesNames, 'c-dropdown_' + modifier, modifier), _defineProperty(_classesNames, 'c-dropdown_size-' + size, size), _defineProperty(_classesNames, className, className), _classesNames));
};

// This is a wrapper for dropdown. So hardly any styling only paddings and max-widths all over the place.
var Dropdown = function Dropdown(_ref2) {
  var children = _ref2.children,
      title = _ref2.title,
      align = _ref2.align,
      rest = _objectWithoutProperties(_ref2, ['children', 'title', 'align']);

  return _react2.default.createElement(
    'div',
    { className: classes(rest) },
    _react2.default.createElement(
      'div',
      { className: 'dropdown__target' },
      title
    ),
    _react2.default.createElement(
      'div',
      { className: (0, _classnames2.default)("c-dropdown__content", _defineProperty({}, 'c-dropdown__alight-' + align + '_content', align)) },
      children
    )
  );
};

Dropdown.propTypes = {
  children: _propTypes2.default.node.isRequired,
  modifier: _propTypes2.default.oneOf([]),
  title: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.node]),
  size: _propTypes2.default.oneOf(['none', 'tiny', 'small', 'large', 'extra-large']),
  align: _propTypes2.default.oneOf(['right'])
};

exports.default = Dropdown;