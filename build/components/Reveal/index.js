'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames2 = require('classnames');

var _classnames3 = _interopRequireDefault(_classnames2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var classes = function classes(_ref) {
  var _classnames;

  var modifier = _ref.modifier,
      className = _ref.className,
      inverted = _ref.inverted,
      size = _ref.size,
      separate = _ref.separate;
  return (0, _classnames3.default)("c-reveal", (_classnames = {}, _defineProperty(_classnames, 'c-reveal_' + modifier, modifier), _defineProperty(_classnames, 'c-reveal_size-' + size, size), _defineProperty(_classnames, className, className), _classnames));
};

// This is a wrapper for reveal. So hardly any styling only paddings and max-widths all over the place.
var Content = function Content(_ref2) {
  var children = _ref2.children,
      content = _ref2.content,
      rest = _objectWithoutProperties(_ref2, ['children', 'content']);

  return _react2.default.createElement(
    'div',
    { className: classes(rest) },
    _react2.default.createElement(
      'div',
      { className: 'c-reveal__content' },
      content
    ),
    _react2.default.createElement(
      'div',
      { className: 'c-reveal__children' },
      children
    )
  );
};

Content.propTypes = {
  children: _propTypes2.default.node.isRequired,
  modifier: _propTypes2.default.string,
  size: _propTypes2.default.oneOf(['small', 'large']),
  inverted: _propTypes2.default.bool,
  separate: _propTypes2.default.bool
};

exports.default = Content;