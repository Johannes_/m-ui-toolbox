'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames2 = require('classnames');

var _classnames3 = _interopRequireDefault(_classnames2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var classes = function classes(modifier, className, inverted, size, separate, inline, maxWidth) {
	var _classnames;

	return (0, _classnames3.default)('c-content', className, (_classnames = {}, _defineProperty(_classnames, 'c-content_' + modifier, modifier), _defineProperty(_classnames, 'c-content_size-' + size, size), _defineProperty(_classnames, 'c-content_max-width-' + maxWidth, maxWidth), _defineProperty(_classnames, 'is-inverted', inverted), _defineProperty(_classnames, 'is-inline', inline), _defineProperty(_classnames, 'is-separated', separate), _classnames));
};

// This is a wrapper for content. So hardly any styling only paddings and max-widths all over the place.
var Content = function Content(_ref) {
	var children = _ref.children,
	    onScroll = _ref.onScroll,
	    modifier = _ref.modifier,
	    size = _ref.size,
	    maxWidth = _ref.maxWidth,
	    inline = _ref.inline,
	    inverted = _ref.inverted,
	    className = _ref.className,
	    separate = _ref.separate;
	return _react2.default.createElement(
		'div',
		{
			className: classes(modifier, className, inverted, size, separate, inline, maxWidth),
			onScroll: onScroll
		},
		children
	);
};

Content.defaultProps = {
	modifier: undefined,
	size: undefined,
	maxWidth: undefined,
	className: undefined,
	inline: undefined,
	inverted: undefined,
	separate: undefined,
	onScroll: undefined
};

Content.propTypes = {
	children: _propTypes2.default.node,
	modifier: _propTypes2.default.oneOf(['only-top', 'fluid', 'only-horizontal', "only-vertical", 'only-right', 'only-left', 'right-bottom', 'only-bottom', 'not-top', 'not-bottom']),
	size: _propTypes2.default.oneOf(['none', 'tiny', 'small', 'medium', 'large', 'extra-large']),
	maxWidth: _propTypes2.default.oneOf(['small', 'medium', 'extra-large', 'exclude-bottom', 'exclude-top']),
	inline: _propTypes2.default.bool,
	inverted: _propTypes2.default.bool,
	separate: _propTypes2.default.bool,
	onScroll: _propTypes2.default.func
};

exports.default = Content;