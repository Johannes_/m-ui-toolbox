'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames2 = require('classnames');

var _classnames3 = _interopRequireDefault(_classnames2);

var _Icon = require('./Icon');

var _Icon2 = _interopRequireDefault(_Icon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// Compoments


// Imports
var alertIcon = function alertIcon(props) {
  return _react2.default.createElement(
    'svg',
    props,
    _react2.default.createElement(
      'title',
      null,
      'icon/small/error'
    ),
    _react2.default.createElement(
      'g',
      {
        fillRule: 'evenodd'
      },
      _react2.default.createElement('path', {
        d: 'M3.23 12.324c-1.101 0-1.56-.785-1.027-1.753l3.579-6.494c.534-.968 1.397-.968 1.929 0l3.564 6.494c.531.968.062 1.753-1.03 1.753H3.23zM6.737 10.824v-1M6.737 5.824v3'
      })
    )
  );
};

alertIcon.defaultProps = {
  width: '13',
  height: '13',
  viewBox: '0 0 13 13',
  xmlns: 'http://www.w3.org/2000/svg'
};

var exitIcon = function exitIcon(props) {
  return _react2.default.createElement(
    'svg',
    props,
    _react2.default.createElement(
      'title',
      null,
      'icon/small/denied'
    ),
    _react2.default.createElement(
      'g',
      {
        stroke: '#000',
        fill: 'none',
        fillRule: 'evenodd',
        strokeLinecap: 'round',
        strokeLinejoin: 'round'
      },
      _react2.default.createElement('path', {
        d: 'M10 3L6 7.036 2 3'
      }),
      _react2.default.createElement('path', {
        d: 'M2 11l4-4.036L10 11'
      })
    )
  );
};

exitIcon.defaultProps = {
  width: '13',
  height: '13',
  viewBox: '0 0 13 13',
  xmlns: 'http://www.w3.org/2000/svg'
};


var classes = function classes(className, level, sub, modifier) {
  var _classnames;

  var clickable = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;
  return (0, _classnames3.default)('c-alert', (_classnames = {}, _defineProperty(_classnames, 'c-alert__level-' + level, level), _defineProperty(_classnames, 'c-alert_' + modifier, modifier), _defineProperty(_classnames, 'c-alert_with-sub', sub), _defineProperty(_classnames, 'js-clickable', clickable), _classnames), className);
};

var Alert = function Alert(_ref) {
  var className = _ref.className,
      _ref$level = _ref.level,
      level = _ref$level === undefined ? 'normal' : _ref$level,
      title = _ref.title,
      subtitle = _ref.subtitle,
      sub = _ref.sub,
      modifier = _ref.modifier,
      _ref$icon = _ref.icon,
      icon = _ref$icon === undefined ? alertIcon : _ref$icon,
      children = _ref.children,
      tag = _ref.tag,
      handleDismiss = _ref.handleDismiss,
      clickable = _ref.clickable;
  return _react2.default.createElement(
    'div',
    { className: classes(className, level, sub, modifier, clickable) },
    _react2.default.createElement(
      'div',
      { className: 'c-alert__content-wrapper' },
      _react2.default.createElement(
        'span',
        { className: 'c-alert__add-on' },
        tag ? _react2.default.createElement(
          'span',
          { className: 'c-alert__tag' },
          tag
        ) : _react2.default.createElement(_Icon2.default, {
          className: 'c-alert__add-on-icon',
          data: icon,
          width: '13px',
          height: '13px',
          theme: 'inverted'
        })
      ),
      _react2.default.createElement(
        'span',
        { className: 'c-alert__content' },
        _react2.default.createElement(
          'div',
          null,
          title
        ),
        _react2.default.createElement(
          'p',
          { className: 'c-alert__value' },
          subtitle
        ),
        sub && _react2.default.createElement(
          'span',
          { className: 'c-alert__sub' },
          sub
        ),
        children
      ),
      handleDismiss && _react2.default.createElement(
        'span',
        { className: 'c-alert__close' },
        _react2.default.createElement(_Icon2.default, {
          className: 'c-alert__add-on-icon',
          data: exitIcon,
          width: '13px',
          height: '13px'
        })
      )
    )
  );
};

Alert.propTypes = {
  className: _propTypes2.default.string,
  icon: _propTypes2.default.bool,
  title: _propTypes2.default.string.isRequired,
  subtitle: _propTypes2.default.string,
  sub: _propTypes2.default.string,
  modifier: _propTypes2.default.oneOf(['not-intrusif', 'letter-status', 'fluid']),
  tag: _propTypes2.default.string,
  handleDismiss: _propTypes2.default.func,
  clickable: _propTypes2.default.bool,
  level: _propTypes2.default.oneOf(['normal', 'visited', 'warning', 'danger', 'success', 'info']),
  children: _propTypes2.default.node
};

exports.default = Alert;