'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var classes = function classes(className) {
  return (0, _classnames2.default)('c-status-bar', className);
};

var StatisticsBar = function StatisticsBar(props) {
  var total = props.total,
      completed = props.completed,
      className = props.className;


  return _react2.default.createElement(
    'div',
    { className: classes(className) },
    _react2.default.createElement('div', {
      className: 'c-status-bar__completed',
      style: { width: completed / total * 100 + '%' }
    })
  );
};

StatisticsBar.propTypes = {
  total: _propTypes2.default.number,
  completed: _propTypes2.default.number
};

exports.default = StatisticsBar;