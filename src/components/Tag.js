import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

// Components
import Icon from './Icon';

const classes = (size, loading, className, theme, modifier) =>
  classNames(
    'c-tag',
    {
      [`c-tag_size-${size}`]: size,
      [`c-tag_theme-${theme}`]: theme,
      [`c-tag_${modifier}`]: modifier
    },
    className
  );

const Tag = props => {
  const {
    children,
    theme = 'normal',
    icon,
    addOn,
    size,
    className,
    modifier,
    title,
    addOnLocationBehind = false,
    style
  } = props;

  const addOns = [];

  if (icon) {
    addOns.push(
      <Icon
        key="tab-icon"
        className="c-tag__icon"
        skipDefaultClass
        data={icon}
        size={size}
      />
    );
  }

  if (addOn) {
    addOns.push(
      <span key="tab-add-on" className="c-tag__add-on">
        {addOn}
      </span>
    );
  }

  return (
    <span className={classes(size, icon, className, theme, modifier)} style={style}>
      {!addOnLocationBehind && addOns}
      <span className="c-tag__body">
        {title}
        {children}
      </span>
      {addOnLocationBehind && addOns}
    </span>
  );
};

Tag.propTypes = {
  theme: PropTypes.oneOf([
    'primary',
    'normal',
    'secondary',
    'danger',
    'warning',
    'info',
    'success'
  ]),
  style: PropTypes.object,
  children: PropTypes.node,
  icon: PropTypes.node,
  modifier: PropTypes.string,
  title: PropTypes.string,
  addOn: PropTypes.string,
  className: PropTypes.string,
  addOnLocationBehind: PropTypes.bool,
  size: PropTypes.oneOf(['small', 'medium', 'large'])
};

export default Tag;
