import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

// Compoments
import Icon from './Icon';

// Imports
import alertIcon from '../../assets/icons/error.svg';
import exitIcon from '../../assets/icons/denied.svg';

const classes = (className, level, sub, modifier, clickable = false) =>
  classnames(
    'c-alert',
    {
      [`c-alert__level-${level}`]: level,
      [`c-alert_${modifier}`]: modifier,
      'c-alert_with-sub': sub,
      'js-clickable': clickable
    },
    className
  );
 
const Alert = ({
  className,
  level = 'normal', 
  title,
  subtitle,
  sub,
  modifier,
  icon = alertIcon,
  children,
  tag,
  handleDismiss,
  clickable
}) => (
  <div className={classes(className, level, sub, modifier, clickable)}>
		<div className="c-alert__content-wrapper">
			<span className="c-alert__add-on">
				{tag ? (
					<span className="c-alert__tag">{tag}</span>
				) : (
					<Icon
						className="c-alert__add-on-icon"
						data={icon}
						width="13px"
						height="13px"
						theme="inverted"
					/>
				)}
			</span>
			<span className="c-alert__content">
				<div>{title}</div>
				<p className="c-alert__value">{subtitle}</p>
				{sub && <span className="c-alert__sub">{sub}</span>}
				{children}
			</span>
			{handleDismiss && (
				<span className="c-alert__close">
					<Icon
						className="c-alert__add-on-icon"
						data={exitIcon}
						width="13px"
						height="13px"
					/>
				</span>
			)}
			</div>
  </div>
);

Alert.propTypes = {
  className: PropTypes.string,
  icon: PropTypes.bool,
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string,
  sub: PropTypes.string,
  modifier: PropTypes.oneOf(['not-intrusif', 'letter-status', 'fluid']),
  tag: PropTypes.string,
  handleDismiss: PropTypes.func,
  clickable: PropTypes.bool,
  level: PropTypes.oneOf([
    'normal',
    'visited',
    'warning',
    'danger',
    'success',
    'info'
  ]),
  children: PropTypes.node
};

export default Alert;
