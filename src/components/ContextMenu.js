import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { size } from 'lodash';
import { FormattedMessage } from 'react-intl';

// Components
import Tooltip from './Tooltip';
import Icon from './Icon';

const classes = (className, block, pullRight) =>
  classNames(
    `c-context-menu`,
    {
      'u-block': block,
      'u-pull-right': pullRight
    },
    className
  );

const actionClasses = ({
  modifier,
  className,
  handleAction,
  isAction = false,
  isActive,
  pullRight
}) =>
  classNames('c-context-menu__item', className, {
    [`c-context-menu_${modifier}_item`]: modifier,
    'c-context-menu_no-action_item': !handleAction && !isAction,
    'c-context-menu_button_item': modifier === 'submit',
    'js-active': isActive
  });
const contentClasses = ({ align = 'left' }) =>
  classNames('c-context-menu__contents', {
    [`c-context-menu_align-${align}_contents`]: align
  });

class ContextMenu extends Component {
  render() {
    const {
      children,
      actionHeader,
      align,
      actions,
      className,
      block,
      form,
      hover = false,
      click = false,
      level = 1,
      renderOnShow,
      pullRight = false
    } = this.props;

    let content = (
      <span>
        {actionHeader && (
          <div className="c-context-menu__header">{actionHeader}</div>
        )}
        {size(actions) > 0 &&
          actions.map((action, key) => {
            let actionContent;

            if (action.modifier === 'submit') {
              actionContent = (
                <div className={actionClasses(action)}>
                  <button
                    type="button"
                    className="c-context-menu__button c-button c-button_primary"
                    onClick={() => {
                      if (!action.preventClose) {
                        // Tooltip on focus will keep the tooltip active. Doing it one frame later solves this.
                        requestAnimationFrame(() => {
                          this.tooltip.hide();
                        });
                      }
                    }}
                  >
                    {action.icon && <Icon data={action.icon} size="small" />}
                    {action.label && action.label.id ? (
                      <FormattedMessage {...action.label} />
                    ) : (
                      action.label
                    )}
                  </button>
                </div>
              );
            } else {
              actionContent = (
                <div
                  className={actionClasses(action)}
                  onClick={e => {
                    if (action.handleAction) {
                      action.handleAction(e);

                      if (!action.preventClose) {
                        this.tooltip.hide();
                      }
                    }
                  }}
                >
                  {action.icon && <Icon data={action.icon} size="small" />}
                  {action.label && action.label.id ? (
                    <FormattedMessage {...action.label} />
                  ) : (
                    action.label
                  )}
                </div>
              );
            }

            if (action.child) {
              return (
                <ContextMenu
                  modifier="inline"
                  key={`context-menu-action-${level}`}
                  block
                  align="right-top"
                  hover
                  click
                  {...action.child}
                  level={level ? level + 1 : 1}
                >
                  {actionContent}
                </ContextMenu>
              );
            }

            return (
              <span key={`context-menu-action-${level}-${key}-${action.id}`}>
                {actionContent}
              </span>
            );
          })}
      </span>
    );

    if (form) {
      content = <form {...form}>{content}</form>;
    }

    return (
      <Tooltip
        size="no-padding"
        content={content}
        className={classes(className, block, pullRight)}
        ref={ref => {
          this.tooltip = ref;
        }}
        hover={hover}
        align={align}
        click={click}
        renderOnShow={renderOnShow}
        contentClassName={contentClasses(this.props)}
      >
        {children}
      </Tooltip>
    );
  }
}

ContextMenu.propTypes = {
  children: PropTypes.node.isRequired,
  actionHeader: PropTypes.string,
  align: PropTypes.string,
  form: PropTypes.object,
  block: PropTypes.bool,
  hover: PropTypes.bool,
  renderOnShow: PropTypes.bool,
  click: PropTypes.bool,
  className: PropTypes.string,
  level: PropTypes.number,
  actions: PropTypes.array.isRequired
};

export default ContextMenu;
