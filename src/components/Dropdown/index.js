import React from 'react';
import PropTypes from 'prop-types';
import classesNames from 'classnames';

const classes = ({ modifier, className, size }) =>
  classesNames("c-dropdown", {
    [`c-dropdown_${modifier}`]: modifier,
    [`c-dropdown_size-${size}`]: size,
    [className]: className
  });

// This is a wrapper for dropdown. So hardly any styling only paddings and max-widths all over the place.
const Dropdown = ({children, title, align, ...rest}) => (
  <div className={classes(rest)}>
    <div className="dropdown__target">
      {title}
    </div>
    <div className={classesNames(
      "c-dropdown__content",
      {
        [`c-dropdown__alight-${align}_content`]: align,
      }
    )}>
      {children}
    </div>
  </div>
);

Dropdown.propTypes = {
  children: PropTypes.node.isRequired,
  modifier: PropTypes.oneOf([]),
  title: PropTypes.oneOfType([ PropTypes.string, PropTypes.node]),
  size: PropTypes.oneOf(['none', 'tiny', 'small', 'large', 'extra-large']),
  align: PropTypes.oneOf(['right']),
};

export default Dropdown;