import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const classes = ({ modifier, className, size }) =>
classNames("c-dropdown__item", className, {
    [`c-dropdown__${modifier}_item`]: modifier,
    [`c-dropdown__size-${size}_item`]: size,

  });

// This is a wrapper for content. So hardly any styling only paddings and max-widths all over the place.
const DropdownItem = ({children, ...rest}) => (
  <div className={classes(rest)}>
    {children}
  </div>
);

DropdownItem.propTypes = {
  children: PropTypes.node.isRequired,
  modifier: PropTypes.string,
  size: PropTypes.oneOf(['none', 'tiny', 'small', 'large', 'extra-large']),
  inline: PropTypes.bool,
  inverted: PropTypes.bool,
  separate: PropTypes.bool,
};

export default DropdownItem;