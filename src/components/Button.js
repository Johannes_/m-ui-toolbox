import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

// Components
import Icon from './Icon';

// Assets
import iconProgress from '../../assets/icons/progress.svg';

const classes = (
  { size, loading, className },
  theme,
  appearance,
  modifier,
	hoverIcon,
	state
) =>
  classNames(
    {
      'c-button': appearance === 'button',
      [`c-button_${size}`]: size !== 'regular',
      'c-button_hover-icon': hoverIcon,
      [`c-button_${modifier}`]: modifier,
      [`c-button_appearance-${appearance}`]: appearance !== 'button',
      [`c-button_theme-${theme}`]: theme !== 'primary',
      'is-loading': loading
    },
		className,
		state
  );

export const themes = [
  'primary',
  'secondary',
  'danger',
  'info',
  'inverted',
	'normal',
	'theme-brand-ghost'
];

export const sizes = ['tiny', 'small', 'regular', 'large', 'medium-rounded'];

export default class Button extends Component {
  render() {
    const {
      disabled,
      loading,
      type,
      children,
      tag,
      theme,
      appearance,
      modifier,
      icon,
      hoverIcon,
      size,
      submitting = false,
			onClick,
			state,
      ...other
    } = this.props;

    let iconComponent = icon && (
      <Icon data={icon} size={size} theme={theme} className="c-button__icon" />
    );

    if (loading) {
      iconComponent = (
        <Icon
          key="active"
          className="c-button__icon"
          skipDefaultClass
          data={iconProgress}
        />
      );
    }

    const wrappedChildren = Boolean(children) && <span className="c-button__children">{children}</span>;

    const Tag = tag;
    return (
      <Tag
        {...other}
        onClick={!disabled ? onClick : undefined}
        className={classes(
          this.props,
          theme,
          appearance,
          modifier,
					hoverIcon,
					state
        )}
        disabled={disabled || loading}
        type={type}
      >
        {submitting ? (
          <span>{wrappedChildren}
            <Icon
              className="u-transform-rotate c-button__icon"
              skipDefaultClass
              data={iconProgress}
            />
          </span>
        ) : (
          <span>{wrappedChildren}
						{hoverIcon && (
              <Icon
                data={hoverIcon}
                size={size}
                theme={theme}
                className="c-button__icon c-button_hover_icon"
              />
            )}
            {iconComponent}
          </span>
        )}
      </Tag>
    );
  }
}

Button.propTypes = {
  tag: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
  appearance: PropTypes.oneOf(['button', 'link']),
  theme: PropTypes.oneOf(themes),
  children: PropTypes.node,
  icon: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
  hoverIcon: PropTypes.node,
  disabled: PropTypes.bool,
  submitting: PropTypes.bool,
  loading: PropTypes.bool,
  size: PropTypes.oneOf(sizes),
  modifier: PropTypes.oneOf(['wide', 'flat']),
  state: PropTypes.oneOf(['static']),
};

Button.defaultProps = {
  tag: 'button',
  appearance: 'button',
  theme: 'primary',
  children: null,
  disabled: false,
  loading: false,
  type: 'submit'
};
