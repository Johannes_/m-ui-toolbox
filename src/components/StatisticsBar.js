import React, { Component } from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

const classes = className => classnames('c-status-bar', className);

const StatisticsBar = props => {
  const { total, completed, className } = props;

  return (
    <div className={classes(className)}>
      <div
        className="c-status-bar__completed"
        style={{ width: `${completed / total * 100}%` }}
      />
    </div>
  );
};

StatisticsBar.propTypes = {
  total: PropTypes.number,
  completed: PropTypes.number
};

export default StatisticsBar;
