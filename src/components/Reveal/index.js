import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const classes = ({ modifier, className, inverted, size, separate}) =>
  classnames("c-reveal", {
    [`c-reveal_${modifier}`]: modifier,
    [`c-reveal_size-${size}`]: size,
    [className]: className
  });

// This is a wrapper for reveal. So hardly any styling only paddings and max-widths all over the place.
const Content = ({children, content, ...rest}) => (
  <div className={classes(rest)}>
    <div className="c-reveal__content">{content}</div>
    <div className="c-reveal__children">{children}</div>
  </div>
);

Content.propTypes = {
  children: PropTypes.node.isRequired,
  modifier: PropTypes.string,
  size: PropTypes.oneOf(['small', 'large']),
  inverted: PropTypes.bool,
  separate: PropTypes.bool,
};

export default Content;