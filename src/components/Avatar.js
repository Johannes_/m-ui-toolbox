import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

// Components
import Icon from './Icon';

// Imports
import userIcon from '../../assets/icons/user.svg';
import starIcon from '../../assets/icons/star.svg';

const classes = (theme, size, wrapperClasses, imageError, modifier, color) =>
  classNames(
    'c-avatar',
    {
      [`c-avatar_${theme}`]: theme,
      [`c-avatar_${modifier}`]: modifier,
      [`c-avatar_size-${size}`]: size,
      'js-image-error': imageError,
      'js-active': color
    },
    wrapperClasses
  );

class Avatar extends Component {
  constructor(props) {
    super(props);

    this.state = { imageError: false };
  }

  handleImageError = () => {
    this.setState({
      imageError: true
    });
  }

  render() {
    const {
      image,
      alt,
      theme,
      size,
      wrapperClasses,
      className = '',
      width,
      height,
      nickname,
      modifier,
      color,
      isSpecial
    } = this.props;
    const addedStyle = {};

    if (color) {
      addedStyle.borderColor = color;
      addedStyle.backgroundColor = color;
    } 

    return (
      <span
        className={classes(
          theme,
          size,
          wrapperClasses,
          this.state.imageError,
          modifier,
          color
        )}
        style={addedStyle}
      >
        {isSpecial && imageError && (
          <Icon
            className="c-avatar__very-special-icon"
            width={width}
            height={height}
            data={starIcon}
            theme={theme}
            size={size}
            style={{ stroke: color }}
          />
        )}
        {image && (
          <img
            onError={this.handleImageError}
            className={`c-avatar__image ${className}`}
            width={width}
            height={height}
            alt={alt}
            src={image}
          />
        )}
      </span>
    );
  }
}

Avatar.propTypes = {
  image: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  theme: PropTypes.oneOf(['inverted']),
  modifier: PropTypes.oneOf(['activatable']),
  size: PropTypes.oneOf(['small', 'large', 'big', 'full']),
  wrapperClasses: PropTypes.string,
  nickname: PropTypes.string,
  alt: PropTypes.string,
  className: PropTypes.string,
  height: PropTypes.string,
  color: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
  isSpecial: PropTypes.bool,
  width: PropTypes.string
};

export default Avatar;
