import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const Cell = ({ children, size, className, modifier, width, justify }) => (
  <div className={classnames( 'c-flex__cell', className,
    {
      [`c-flex__size-${size}_cell`]: size,
      [`c-flex__width-${width}_cell`]: width,
      [`c-flex__justify-${justify}_cell`]: justify,
      [`c-flex__${modifier}_cell`]: modifier,
    })}
  >
    {children}
  </div>
);

Cell.propTypes = {
  children: PropTypes.node,
  size: PropTypes.oneOf(['small', 'large']),
  className: PropTypes.string,
  width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  modifier: PropTypes.oneOf(['not-grow', 'not-shrink']),
  justify: PropTypes.oneOf(['center']),
  padded: PropTypes.bool
};

export default Cell;
