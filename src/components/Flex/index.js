import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const Wrapper = ({ children, size, className, wrap, collapse, modifier, align, noPadding }) => (
  <div className={classnames( 'c-flex', className,
    {
      [`c-flex_size-${size}`]: size,
      [`c-flex_collapse-${collapse}`]: collapse,
      [`c-flex_${modifier}`]: modifier,
      [`c-flex_align-${align}`]: align,
      'c-flex_wrap': wrap,
      'c-flex_no-padding': noPadding,
    })}
  >
    {children}
  </div>
);

Wrapper.propTypes = {
  children: PropTypes.node,
  size: PropTypes.oneOf(['small', 'large']),
  className: PropTypes.string,
  wrap: PropTypes.bool,
  modifier: PropTypes.oneOf(['row', 'centered-row', 'row-reversed', 'block', 'stretch', 'horizontal-separated']),
  noPadding: PropTypes.bool,
  align: PropTypes.oneOf(['flex-start']),
  collapse: PropTypes.oneOf(['small-1', 'small-2', 'large-1']),
};

export default Wrapper;
