import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const classes = (modifier, className) =>
  classnames("c-card", {
    [`c-card_${modifier}`]: modifier,
    [className]: className
  });

const Card = ({children, className, modifier}) => (
  <div className={classes(modifier, className)}>
    {children}
  </div>
);

Card.propTypes = {
  children: PropTypes.node.isRequired,
  modifier: PropTypes.oneOf(['as-link', 'fluid', 'bordered-content', 'bordered', 'inset', 'scale-down'])
};

export default Card;