import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const classes = (className, modifier) =>
  classNames(
    'c-tag-group',
    {
      [`c-tag-group_${modifier}`]: modifier
    },
    className
  );

const ButtonGroup = props => {
  const { children, className, modifier } = props;

  return <span className={classes(className, modifier)}>{children}</span>;
};

ButtonGroup.propTypes = {
  children: PropTypes.node,
  modifier: PropTypes.string,
  className: PropTypes.string
};

export default ButtonGroup;
