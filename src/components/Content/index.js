import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const classes = (
	modifier, className, inverted, size, separate, inline, maxWidth,
) =>
	classnames(
		'c-content',
		className, {
			[`c-content_${modifier}`]: modifier,
			[`c-content_size-${size}`]: size,
			[`c-content_max-width-${maxWidth}`]: maxWidth,
			'is-inverted': inverted,
			'is-inline': inline,
			'is-separated': separate,
		},
	);

// This is a wrapper for content. So hardly any styling only paddings and max-widths all over the place.
const Content = ({
	children,
	onScroll,
	modifier,
	size,
	maxWidth,
	inline,
	inverted,
	className,
	separate,
}) => (
	<div
		className={classes(
			modifier,
			className,
			inverted,
			size,
			separate,
			inline,
			maxWidth,
		)}
		onScroll={onScroll}
	>
		{children}
	</div>
);

Content.defaultProps = {
	modifier: undefined,
	size: undefined,
	maxWidth: undefined,
	className: undefined,
	inline: undefined,
	inverted: undefined,
	separate: undefined,
	onScroll: undefined,
};

Content.propTypes = {
  children: PropTypes.node,
  modifier: PropTypes.oneOf(['only-top', 'fluid', 'only-horizontal', "only-vertical", 'only-right', 'only-left', 'right-bottom', 'only-bottom', 'not-top', 'not-bottom']),
  size: PropTypes.oneOf(['none', 'tiny', 'small', 'medium', 'large', 'extra-large']),
  maxWidth: PropTypes.oneOf(['small', 'medium', 'extra-large', 'exclude-bottom', 'exclude-top']),
  inline: PropTypes.bool,
  inverted: PropTypes.bool,
  separate: PropTypes.bool,
  onScroll: PropTypes.func,
};

export default Content;
