import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import classNames from 'classnames';
import { isEmpty } from 'lodash';

// Input: DOM element
// Returns: { int offsetLeft, int offsetTop }
// Determines how much the flexed parents have offset top and left.
export const findModalAndGetOffset = element => {
  let offsetLeft = 0;
  let offsetTop = 0;
  let foundResult = false;
  let currentElement = element;
  do {
    // eslint-disable-line
    if (
      currentElement.className &&
      currentElement.className.indexOf('c-modal__content') !== -1
    ) {
      offsetTop = currentElement.offsetTop;
      offsetLeft = currentElement.offsetLeft;

      foundResult = true;
    }
  } while (
    (currentElement = currentElement.parentNode) !== null &&
    !foundResult
  );
  return { offsetLeft, offsetTop };
};

export const findScrollOffset = element => {
  let scrollLeft = 0;
  let scrollTop = 0;
  let foundResult = false;
  let currentElement = element;
  do {
    if (currentElement.parentNode) {
      const elementRect = ReactDOM.findDOMNode(currentElement);
      scrollTop = scrollTop + elementRect.scrollTop;
      scrollLeft = scrollLeft + elementRect.scrollLeft;
    }
  } while ((currentElement = currentElement.parentNode) !== null);
  return { scrollLeft, scrollTop };
};

// Fist place the tooltip below the children.
// If fails try above.
// TODO: Split the position up for every possible combination (this is a mess).
const getPosition = (tooltip, contextMenu, alignProp) => {
  const tooltipElement = ReactDOM.findDOMNode(tooltip).getBoundingClientRect();
  const contextMenuElement = ReactDOM.findDOMNode(
    contextMenu
  ).getBoundingClientRect();

  let top = tooltipElement.top + tooltipElement.height;
  let left = tooltipElement.left + tooltipElement.width / 2;

  if (alignProp === 'right') {
    left = tooltipElement.left + tooltipElement.width;
  }

  if (alignProp === 'right-top') {
    left = tooltipElement.left + tooltipElement.width;
    top = tooltipElement.top;
  }

  let align = 'center';

  if (
    contextMenuElement.height + tooltipElement.height + tooltipElement.top >
    document.documentElement.clientHeight
  ) {
    top = tooltipElement.top - contextMenuElement.height;
    align = 'center-bottom';

    if (alignProp === 'right-top') {
      top += tooltipElement.height * 2;
    }
  }

  const { offsetLeft, offsetTop } = findModalAndGetOffset(tooltip);

  // Also move the drop down element to the left if not all of the element is on the screen.
  const pickerRightBorderLocation =
    contextMenuElement.left + contextMenuElement.width;

  if (document.documentElement.clientWidth - pickerRightBorderLocation < 0) {
    left -= document.documentElement.clientWidth - pickerRightBorderLocation;
  }

  return {
    align,
    style: {
      left: Math.round(left - offsetLeft),
      top: Math.round(top - offsetTop)
    }
  };
};

const classes = (
  { size, inverse, className, location, inline, click },
  align,
  active = false
) =>
  classNames('c-tooltip', className, {
    [`c-tooltip_align-${align}`]: align,
    [`c-tooltip_location-${location}`]: location !== 'bottom',
    [`c-tooltip_${size}`]: size !== 'regular',
    'c-tooltip_inverse': inverse,
    'c-tooltip_inline': inline,
    'js-clickable': click,
    'js-active': active
  });

class Tooltip extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentWillMount() {
    if (!this.props.hover) {
      document.addEventListener('click', this.handleDocumentClick);
      document.addEventListener('contextmenu', this.handleDocumentClick);
      document.addEventListener('scroll', this.handleDocumentClick);
    }
  }

  componentDidMount() {
    if (this.props.content) {
      this.setPosition();
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.content && nextProps.content !== this.props.content) {
      this.setPosition();
    }
  }

  componentWillUnmount() {
    if (!this.props.hover) {
      document.removeEventListener('click', this.handleDocumentClick);
      document.removeEventListener('contextmenu', this.handleDocumentClick);
      document.removeEventListener('scroll', this.handleDocumentClick);
    }
  }

  setPosition = () => {
    const { align, style } = getPosition(
      ReactDOM.findDOMNode(this.refTooltip),
      this.contextMenu,
      this.props.align
    );

    this.setState({
      align,
      style
    });
  }

  setLocation = (e) => {
    const contextMenu = ReactDOM.findDOMNode(this.contextMenu);

    let top = e.nativeEvent.clientY;
    let align = 'center';

    if (
      contextMenu.getBoundingClientRect().height + e.nativeEvent.clientY >
      document.documentElement.clientHeight
    ) {
      top = e.nativeEvent.clientY - contextMenu.getBoundingClientRect().height;
      align = 'center-bottom';
    }

    this.setState({
      style: {
        left: Math.round(e.nativeEvent.clientX),
        top: Math.round(top)
      },
      align
    });
  }

  setLocationBySelf = (location) => {
    const { scrollLeft, scrollTop } = findScrollOffset(
      ReactDOM.findDOMNode(this.refTooltip)
    );

    switch (location) {
      case 'right': {
        this.setState({
          style: {
            left: Math.round(this.refTooltip.clientWidth - scrollLeft),
            top: 'inherit',
            marginTop: Math.round(
              -(
                this.contextMenu.clientHeight / 2 +
                this.refTooltip.clientHeight / 2 +
                scrollTop
              )
            )
          },
          align: 'right'
        });
      }
    }
  }

  handleBlur = () => {
    if (this.props.hover) {
      this.willHide = setTimeout(() => {
        this.hide();
      }, this.props.delayHide);
    }
  }

  handleFocus = () => {
    this.show();
  }

  handleClick = (e) => {
    if (this.props.click) {
      this.show(e);
    }
  }

  handleMouseOver = () => {
    const { delayShow } = this.props;

    if (!this.props.hover) {
      return;
    }

    if (!delayShow) {
      this.show();
      return;
    }

    this.willShow = setTimeout(() => {
      this.show();
    }, delayShow);
  }

  handleMouseOut = () => {
    const { delayHide } = this.props;

    if (!this.props.hover) {
      return;
    }

    if (!delayHide) {
      this.hide();
      return;
    }

    this.willHide = setTimeout(() => {
      this.hide();
    }, this.props.delayHide);
  }

  handleDocumentClick = (e) => {
    const area = ReactDOM.findDOMNode(this.refTooltip);

    if (!isEmpty(area) && !area.contains(e.target)) {
      this.hide();
    }
  }

  handleContextMenu = (e) => {
    e.preventDefault();

    this.show(e);
  }

  hide = () => {
    if (this.willShow) {
      clearTimeout(this.willShow);
    }

    this.setState({ visible: false });
  }

  show = (e) => {
    if (this.willHide) {
      clearTimeout(this.willHide);
    }

    // If click is disabled but a focus or any other event triggers show this should not come to .
    if (!this.props.click && !e) {
      return;
    }

    // The browser width of anything can change in the mean while so we need to recheck when the tooltip is shown.
    if (e && !this.props.click) {
      // Check if you are clicking the context menu, then we don't need to change anything.
      if (!this.contextMenu.contains(e.target)) {
        this.setLocation(e);
      }
    } else {
      this.setPosition();
      if (this.props.location) {
        this.setLocationBySelf(this.props.location);
      }
    }

    this.setState({ visible: true });
  }

  render() {
    const {
      children,
      content,
      contentClassName,
      align: alignProps,
      renderOnShow,
      className
    } = this.props;
    const { align, style } = this.state;
    return (
      <div
        ref={ref => {
          this.refTooltip = ref;
        }}
        className={classes(this.props, alignProps || align, this.state.visible)}
        onMouseOver={this.handleMouseOver}
        onMouseLeave={this.handleMouseOut}
        onFocus={this.handleFocus}
        onBlur={this.handleBlur}
        onClick={this.handleClick}
        onContextMenu={this.handleContextMenu}
      >
        {children}

        <div
          className={classNames('c-tooltip__contents', contentClassName)}
          role="tooltip"
          aria-hidden={!this.state.visible || !content}
          style={style}
          ref={ref => {
            this.contextMenu = ref;
          }}
        >
          <div className="c-tooltip__content-wrapper">
            {(!renderOnShow || (renderOnShow && this.state.visible)) && content}
          </div>
        </div>
      </div>
    );
  }
}

Tooltip.propTypes = {
  children: PropTypes.node.isRequired,
  content: PropTypes.node,
  delayShow: PropTypes.number,
  delayHide: PropTypes.number,
  hover: PropTypes.bool,
  renderOnShow: PropTypes.bool,
  inline: PropTypes.bool, // eslint-disable-line
  click: PropTypes.bool,
  inverse: PropTypes.bool, // eslint-disable-line
  className: PropTypes.string, // eslint-disable-line
  contentClassName: PropTypes.string,
  align: PropTypes.oneOf(['right', 'center', 'right-top', 'left']),
  size: PropTypes.oneOf(['no-padding', 'small', 'regular', 'big']), // eslint-disable-line
  location: PropTypes.oneOf(['bottom', 'right', 'right-bottom']) // eslint-disable-line
};

Tooltip.defaultProps = {
  delayHide: 200,
  size: 'regular',
  hover: true,
  click: true,
  inline: false,
  inverse: false,
  closeOnBlur: true,
  renderOnShow: false,
  location: 'bottom'
};

export default Tooltip;
