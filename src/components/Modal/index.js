import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactModal from 'react-modal';
import classNames from 'classnames';

const classes = (className, size) => classNames('c-modal__content', className, {
  [`c-modal_${size}_content`]: size
});


const overlayClasses = (overlayClassName, position) =>
  classNames('c-modal', overlayClassName, {
    [`c-modal_position-${position}`]: position
  });

class Modal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      savedOffsetX: 0,
      savedOffsetY: 0
    };
  }

  render() {
    const {
      children,
      className,
      isOpen,
      overlayClassName,
      portalClassName,
      position,
      offsetX = 0,
      offsetY = 0,
      handleClose,
      size,
      ...otherProps
    } = this.props;

    return (
      <ReactModal
        className={classes(className, size)}
        overlayClassName={overlayClasses(overlayClassName, position)}
        portalClassName={classNames('c-modal__portal', portalClassName)}
        onRequestClose={handleClose}
        style={{
          overlay: {
            left: offsetX > 0 && offsetX,
            top: offsetY > 0 && offsetY
          }
        }}
        isOpen={isOpen}
        contentLabel="content"
        {...otherProps}
      >
        {children}
      </ReactModal>
    );
  }
}

Modal.propTypes = {
  children: PropTypes.node.isRequired,
  isOpen: PropTypes.bool.isRequired,
  className: PropTypes.string,
  overlayClassName: PropTypes.string,
  offsetX: PropTypes.number,
  offsetY: PropTypes.number,
  portalClassName: PropTypes.string,
  handleClose: PropTypes.func,
  size: PropTypes.oneOf(['small']),
  position: PropTypes.oneOf(['left']) // default is centered
};

export default Modal;
