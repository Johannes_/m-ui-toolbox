import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import crossIcon from '../../../assets/icons/md-cross.svg';

import FlexCell from '../Flex/Cell';
import FlexWrapper from '../Flex';
import Content from '../Content';
import Button from '../Button';


class ModalHeader extends Component {
  render() {
    const { title, subTitle, handleClose } = this.props;

    return (
      <FlexWrapper noPadding align="flex-start">
				<FlexCell>
					<Content size="medium">
						{<h3 className="u-no-margin">
							<FormattedMessage {...(title)} />
						</h3>}

						{subTitle}
					</Content>
				</FlexCell>
				<FlexCell modifier="not-grow">
					<Content size="medium">
						<Button icon={crossIcon} onClick={handleClose} appearance="link" />
					</Content>
				</FlexCell>
			</FlexWrapper>
    );
  }
}

ModalHeader.propTypes = {
	title: PropTypes.object,
	handleClose: PropTypes.func,
  subTitle: PropTypes.node,
};

export default ModalHeader;
