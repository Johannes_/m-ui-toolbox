import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import SVGInline from 'react-svg-inline';
import classnames from 'classnames';

const classes = (className, theme, size, modifier) =>
  classnames(
    'c-icon',
    {
      [`c-icon_${theme}`]: theme !== 'primary',
      [`c-icon_size-${size}`]: size,
      [`c-icon_${modifier}`]: modifier
    },
    className
  );

const Icon = ({
  data,
  className = '',
  style,
  theme,
  size,
  width,
  height,
  modifier
}) => {
  if (typeof data === "string") {
    if (data.startsWith('/static') || data.startsWith('data:image/svg') || data.endsWith('.svg')) {

      return (
        <img 
          src={data} 
          className={classes(className, theme, size, modifier)} 
          style={style}
          width={width}
          height={height}
        />
        );
    }
    return (
      <SVGInline
        className={classes(className, theme, size, modifier)}
        svg={data}
        style={style}
        width={width}
        height={height}
      />
    );
  } 
  if (data) {
    return <Fragment><span className={classes(className, theme, size, modifier)}>{data()}</span></Fragment>;
  }
  return null;
}

Icon.propTypes = {
  data: PropTypes.oneOfType([PropTypes.string, PropTypes.func]).isRequired,
  width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  modifier: PropTypes.string,
  theme: PropTypes.oneOf([
    'primary',
    'secondary',
    'normal',
    'inline',
    'danger',
		'normal-inline',
		'rotate'
  ]),
  size: PropTypes.oneOf(['tiny', 'small', 'large', 'medium-rounded', 'full', 'extra-large']),
  className: PropTypes.string,
  style: PropTypes.object
};
export default Icon;
