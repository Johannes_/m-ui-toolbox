import React from 'react';
import { injectIntl } from 'react-intl';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const classes = (className, modifier, size) =>
  classNames('c-add-on', className, {
    [`c-add-on_${modifier}`]: modifier,
    [`c-add-on_size-${size}`]: size
  });

const AddOn = props => {
  const {
    name,
    children,
    data,
    modifier,
    size,
    placeholder,
    className,
    position = 'left',
    intl: { formatMessage }
  } = props;

  const placeholderProp =
    placeholder && placeholder.hasOwnProperty('id')
      ? formatMessage(placeholder)
      : placeholder;

  const content = (
    <span
      className={classes(className, modifier, size)}
      id={`c-add-on-${name}`}
      name={name}
      placeholder={placeholderProp}
    >
      {data}
    </span>
  );

  return position === 'left' ? (
    <span className="c-add-on-wrapper">
      {content}
      {children}
    </span>
  ) : (
    <span className="c-add-on-wrapper">
      {children}
      {content}
    </span>
  );
};

AddOn.propTypes = {
  className: PropTypes.string,
  placeholder: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
    PropTypes.number,
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      defaultMessage: PropTypes.string.isRequired
    })
  ]),
  name: PropTypes.string,
  modifier: PropTypes.string,
  data: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  position: PropTypes.oneOf(['left', 'right']),
  intl: PropTypes.object
};

export default injectIntl(AddOn);
